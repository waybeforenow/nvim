local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end

vim.opt.rtp:prepend(lazypath)

local modules = {
    {
        "dracula/vim",
        lazy = false,
        init = function()
            vim.cmd("colorscheme dracula")
        end,
    },
    {
        "preservim/nerdtree",
        init = function()
            vim.cmd([=[
                " autoopen for NERDTree
                autocmd StdinReadPre * let s:std_in=1
                autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
                let NERDTreeIgnore = ['\.o$', '\.exe$', '\.ilk$', '\.pdb$', '\.dll$', '^\.git$[[dir]]']
                let NERDTreeShowHidden=1

                " If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
                autocmd BufEnter * if winnr() == winnr('h') && bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
                    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif
            ]=])
        end,
    },
    "tpope/vim-sensible",
    {
        "sbdchd/neoformat",
        config = function()
            vim.cmd([[
                " options for neoformat
                let g:neoformat_enabled_python = ['black']
                let g:neoformat_cpp_clangformat = {
                    \ 'exe': 'clang-format',
                    \ 'stdin': 1,
                    \ }
                let g:neoformat_enabled_cpp = ['clangformat']

                function! NeoformatAutoEnable()
                    augroup NeoformatOnSave
                        autocmd!
                        autocmd BufWritePre * Neoformat
                    augroup END
                endfunction

                " enable autoformat by default
                call NeoformatAutoEnable()
                command! -nargs=0 NeoformatAutoEnable :call NeoformatAutoEnable()

                function! NeoformatAutoDisable()
                    augroup NeoformatOnSave
                        autocmd!
                    augroup END
                endfunction

                command! -nargs=0 NeoformatAutoDisable :call NeoformatAutoDisable()
            ]])
        end,
    },
    "vim-airline/vim-airline",
    {
        "Yggdroot/indentLine",
        config = function()
            vim.cmd([[
                " options for indentLine
                let g:indentLine_defaultGroup = 'Comment'
                let g:indentLine_enabled = 0
                let g:indentLine_leadingSpaceEnabled = 1
                let g:indentLine_leadingSpaceChar = '·'
            ]])
        end,
    },
    {
        "dhruvasagar/vim-table-mode",
        config = function()
            vim.cmd([[
                let g:table_mode_corner_corner='+'
                let g:table_mode_header_fillchar='='
            ]])
        end,
    },
    "tpope/vim-fugitive",
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            {
                "hrsh7th/nvim-cmp",
                dependencies = {
                    "hrsh7th/cmp-nvim-lsp",
                    "hrsh7th/cmp-buffer",
                    "hrsh7th/cmp-path",
                    "hrsh7th/cmp-cmdline",
                    {
                        "hrsh7th/cmp-vsnip",
                        dependencies = { "hrsh7th/vim-vsnip" }
                    }
                },
                config = function()
                    -- Set up nvim-cmp.
                    local cmp = require("cmp")

                    cmp.setup({
                        snippet = {
                            -- REQUIRED - you must specify a snippet engine
                            expand = function(args)
                                vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
                            end,
                        },
                        window = {
                            -- completion = cmp.config.window.bordered(),
                            -- documentation = cmp.config.window.bordered(),
                        },
                        mapping = cmp.mapping.preset.insert({
                            ["<Tab>"] = cmp.mapping(function(fallback)
                                if cmp.visible() then
                                    cmp.select_next_item()
                                elseif vim.fn["vsnip#available"](1) == 1 then
                                    feedkey("<Plug>(vsnip-expand-or-jump)", "")
                                else
                                    fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
                                end
                            end, { "i", "s" }),
                            ["<S-Tab>"] = cmp.mapping(function()
                                if cmp.visible() then
                                    cmp.select_prev_item()
                                elseif vim.fn["vsnip#jumpable"](-1) == 1 then
                                    feedkey("<Plug>(vsnip-jump-prev)", "")
                                end
                            end, { "i", "s" }),
                            ["<C-Space>"] = cmp.mapping.complete(),
                            ["<C-e>"] = cmp.mapping.abort(),
                            ["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
                        }),
                        sources = cmp.config.sources({
                            { name = "nvim_lsp" },
                            { name = "vsnip" },
                        }, {
                            { name = "buffer" },
                        }),
                    })

                    -- Set configuration for specific filetype.
                    cmp.setup.filetype("gitcommit", {
                        sources = cmp.config.sources({
                            { name = "git" }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
                        }, {
                            { name = "buffer" },
                        }),
                    })

                    -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
                    cmp.setup.cmdline({ "/", "?" }, {
                        mapping = cmp.mapping.preset.cmdline(),
                        sources = {
                            { name = "buffer" },
                        },
                    })

                    -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
                    cmp.setup.cmdline(":", {
                        mapping = cmp.mapping.preset.cmdline(),
                        sources = cmp.config.sources({
                            { name = "path" },
                        }, {
                            { name = "cmdline" },
                        }),
                    })
                end,
            },
        },
        config = function()
            local lspconfig = require("lspconfig")
            local capabilities = require("cmp_nvim_lsp").default_capabilities()

            lspconfig.clangd.setup({ capabilities = capabilities })
            lspconfig.cmake.setup({ capabilities = capabilities })
            lspconfig.pylsp.setup({
                capabilities = capabilities,
                settings = {
                    pylsp = {
                        plugins = {
                            pycodestyle = {
                                ignore = { "E501" },
                            },
                        },
                    },
                },
            })
            lspconfig.esbonio.setup({ capabilities = capabilities })
            lspconfig.rust_analyzer.setup({ capabilities = capabilities })

            lspconfig.lua_ls.setup({
                capabilities = capabilities,
                on_init = function(client)
                    local path = client.workspace_folders[1].name
                    if
                        not vim.loop.fs_stat(path .. "/.luarc.json")
                        and not vim.loop.fs_stat(path .. "/.luarc.jsonc")
                    then
                        client.config.settings = vim.tbl_deep_extend(
                            "force",
                            client.config.settings,
                            {
                                Lua = {
                                    runtime = {
                                        -- Tell the language server which version of Lua you're using
                                        -- (most likely LuaJIT in the case of Neovim)
                                        version = "LuaJIT",
                                    },
                                    -- Make the server aware of Neovim runtime files
                                    workspace = {
                                        checkThirdParty = false,
                                        library = {
                                            vim.env.VIMRUNTIME,
                                            -- "${3rd}/luv/library"
                                            -- "${3rd}/busted/library",
                                        },
                                        -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
                                        -- library = vim.api.nvim_get_runtime_file("", true)
                                    },
                                },
                            }
                        )

                        client.notify(
                            "workspace/didChangeConfiguration",
                            { settings = client.config.settings }
                        )
                    end
                    return true
                end,
            })

            -- Global mappings.
            -- See `:help vim.diagnostic.*` for documentation on any of the below functions
            vim.keymap.set("n", "<space>a", vim.diagnostic.open_float)
            vim.keymap.set("n", "[d", vim.diagnostic.goto_prev)
            vim.keymap.set("n", "]d", vim.diagnostic.goto_next)
            vim.keymap.set("n", "<space>q", vim.diagnostic.setloclist)

            -- Use LspAttach autocommand to only map the following keys
            -- after the language server attaches to the current buffer
            vim.api.nvim_create_autocmd("LspAttach", {
                group = vim.api.nvim_create_augroup("UserLspConfig", {}),
                callback = function(ev)
                    -- Enable completion triggered by <c-x><c-o>
                    vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

                    -- Buffer local mappings.
                    -- See `:help vim.lsp.*` for documentation on any of the below functions
                    local opts = { buffer = ev.buf }
                    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
                    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
                    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
                    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
                    vim.keymap.set(
                        "n",
                        "<C-k>",
                        vim.lsp.buf.signature_help,
                        opts
                    )
                    vim.keymap.set(
                        "n",
                        "<space>wa",
                        vim.lsp.buf.add_workspace_folder,
                        opts
                    )
                    vim.keymap.set(
                        "n",
                        "<space>wr",
                        vim.lsp.buf.remove_workspace_folder,
                        opts
                    )
                    vim.keymap.set("n", "<space>wl", function()
                        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
                    end, opts)
                    vim.keymap.set(
                        "n",
                        "<space>D",
                        vim.lsp.buf.type_definition,
                        opts
                    )
                    vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, opts)
                    vim.keymap.set(
                        { "n", "v" },
                        "<space>ca",
                        vim.lsp.buf.code_action,
                        opts
                    )
                    vim.keymap.set("n", "<space>f", function()
                        vim.lsp.buf.format({ async = true })
                    end, opts)
                end,
            })
        end,
    },
    {
        "nvim-treesitter/nvim-treesitter",
        config = function()
            require("nvim-treesitter.configs").setup({
                ensure_installed = {
                    "c",
                    "lua",
                    "vim",
                    "vimdoc",
                    "query",
                    "cpp",
                },
                sync_install = false,
                auto_install = true,
                highlight = {
                    enable = true,
                    disable = { "markdown", "markdown_inline" },
                    additional_vim_regex_highlighting = false,
                },
            })
        end,
    },
    {
        "nvim-telescope/telescope.nvim",
        tag = "0.1.4",
        dependencies = { "nvim-lua/plenary.nvim" },
        config = function()
            local builtin = require("telescope.builtin")
            vim.keymap.set("n", ",f", builtin.find_files, {})
            vim.keymap.set("n", ",g", builtin.live_grep, {})
            vim.keymap.set("n", ",b", builtin.buffers, {})
            vim.keymap.set("n", ",h", builtin.help_tags, {})
            vim.keymap.set("n", "gr", builtin.lsp_references, {})
        end,
    },
    {
        "lewis6991/gitsigns.nvim",
        opts = {
            signs = {
                add = { text = "+" },
                change = { text = "~" },
                delete = { text = "-" },
                topdelete = { text = "-" },
                changedelete = { text = "~" },
                untracked = { text = " " },
            },
            signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
            numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
            linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
            word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
            watch_gitdir = {
                follow_files = true,
            },
            attach_to_untracked = true,
            current_line_blame = false,
            sign_priority = 6,
            update_debounce = 100,
            status_formatter = nil, -- Use default
            max_file_length = 40000, -- Disable if file is longer than this (in lines)
            preview_config = {
                -- Options passed to nvim_open_win
                border = "single",
                style = "minimal",
                relative = "cursor",
                row = 0,
                col = 1,
            },
        },
    },
}

require("lazy").setup(modules)

vim.cmd([[
" baseline
set langmenu=en_US.UTF-8        " sets the language of the menu (gvim)
syntax on
filetype plugin indent on
set number
set encoding=utf-8
set colorcolumn=81
set fileformats=unix,dos

" tab fun
set tabstop=4
set shiftwidth=4
set expandtab
set cino=N-s,g0 " for C++ namespace{} declarations and visibility keywords

" color scheme
set background=dark

" gVim font
if has("win32")
    set guifont=Consolas:h10
endif

" MacVim font
if has("gui_macvim")
    set guifont=Monaco:h12
endif

" disable most mouse things
set mouse=c

autocmd FileType scheme setlocal tabstop=2 shiftwidth=2
]])

vim.keymap.set("n", ",,", "<cmd>nohlsearch<cr>")
vim.api.nvim_set_hl(0, "GitSignsAdd", { link = "NonText" })
vim.api.nvim_set_hl(0, "GitSignsChange", { link = "NonText" })
vim.api.nvim_set_hl(0, "GitSignsChangedelete", { link = "NonText" })
vim.api.nvim_set_hl(0, "GitSignsDelete", { link = "NonText" })
vim.api.nvim_set_hl(0, "GitSignsTopdelete", { link = "NonText" })
vim.api.nvim_set_hl(0, "GitSignsUntracked", { link = "NonText" })
